;<?php/*

[general]
name="LibTranslate"
version="1.12.1"
encoding="UTF-8"
description="Translate module"
description.fr="Librairie partagée permettant le gestion des traductions au format gettext"
long_description.fr="README.md"
delete=0
ov_version="8.1.101"
php_version="7.1"
mysql_character_set_database="latin1,utf8"
addon_access_control="0"
author="Robin Bailleux (robin.bailleux@si-4you.com)"
icon="icon.png"
tags="library,default"

;*/?>