<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

class Func_Translate extends bab_functionality 
{
	/**
	 *
	 * @var string
	 */
	protected $addonName;
	
	/**
	 * 
	 * @var string
	 */
	protected $defaultLanguage = 'en';
	
	protected $langPath;
	
	
	public function getDescription()
	{
		return 'Zend translate';
	}
	
	/**
	 * 
	 * @param string $name
	 * @return Func_Translate
	 */
	public function setAddonName($name)
	{
		$this->addonName = $name;
		return $this;
	}
	
	/**
	 * Set default language to use if the transaltions for user language are not found
	 * @param string $code
	 * @return Func_Translate
	 */
	public function setDefaultLanguage($code)
	{
		$this->defaultLanguage = $code;
		return $this;
	}
	
	
	
	/**
	 * Set the zend path into the include path
	 * @return boolean
	 */
	protected function setIncludePath() {
	
		// test if zend is allready included
	
		$new_path = dirname(dirname(__FILE__));
		$includepath = explode(PATH_SEPARATOR, get_include_path());
	
		if (!in_array($new_path, $includepath)) {
			array_unshift($includepath, $new_path);
		}
		if (false === set_include_path(implode(PATH_SEPARATOR, $includepath))) {
			return false;
		}
	
		return true;
	}
	
	
	
	/**
	 * Require the Zend framework translate module
	 * Use from the LibZendFramework addon in priority 
	 * or use embeded files if not present
	 * 
	 * @return bool
	 */
	public function requireZendModule()
	{
		if (class_exists('Zend_Translate', false))
		{
			return true;
		}
		
		$func = @bab_functionality::get('ZendFramework', true);
		
		if (false !== $func) {
			$func->setIncludePath();
			require_once 'Zend/Translate.php';
			return true;
		}
		
		$this->setIncludePath();
		require_once dirname(__FILE__).'/../Zend/Translate.php';
		return true;
	}
	
	
	/**
	 * Get the lang path in Ovidentia
	 * @return string
	 */
	protected function getLangPath()
	{
	    if(isset($this->langPath)){
	        return $this->langPath;
	    }
	    $this->setLangPath($GLOBALS['babInstallPath'].'lang/');
		if (isset($this->addonName))
		{
		    $addon = bab_getAddonInfosInstance($this->addonName);
		    if ($addon) {
		        $this->setLangPath($addon->getLangPath());
		    }
		}
		
		return $this->langPath;
	}
	
	public function setLangPath($langPath){
	    $this->langPath = $langPath;
	    return $this;
	}
	
	
	
	/**
	 * Test if there are translations for language
	 * @param string $language
	 * @return bool
	 */
	protected function translationsExists($language)
	{
		throw new Exception('Not implemented');
	}
	
	
	/**
	 * Translate
	 * 
	 * @param string $str				Text to translate or singular form
	 * @param string $str_plurals		Plurals form string
	 * @param unknown_type $number		Number of items for plurals
	 * 
	 * @return string
	 */
	public function translate($str, $str_plurals = null, $number = null)
	{
		throw new Exception('Not implemented');
	}
	
}