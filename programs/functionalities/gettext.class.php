<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

bab_functionality::includeOriginal('Translate');

class Func_Translate_Gettext extends Func_Translate
{
    /**
     * Storage for one adapter per addon or core
     * @var Zend_Translate[]
     */
    private $adapters = array();

    /**
     * @var string $userLanguage
     */
    private $userLanguage = null;


    /**
     * {@inheritDoc}
     * @see Func_Translate::getDescription()
     */
    public function getDescription()
    {
        return 'Gettext adapter';
    }

    /**
     * (non-PHPdoc)
     * @see Func_Translate::translationsExists()
     *
     * @return bool
     */
    protected function translationsExists($language)
    {
        $file = $this->getLangPath().'lang-'.$language.'.mo';
        return file_exists($file);
    }


    /**
     * Forces the use a specific language for translations.
     *
     * @param string $language
     */
    public function setLanguage($language)
    {
        if ($language != $this->userLanguage) {
           $this->userLanguage = $language;
           $this->adapters = array();
        }
        return $this;
    }
    
    public function getLanguage()
    {
        return $this->userLanguage;
    }


    /**
     * @return Zend_Translate | null
     */
    protected function getAdapter()
    {
        if (!isset($this->adapters[$this->addonName])) {
            $this->requireZendModule();
            if (!isset($this->userLanguage)) {
                $this->userLanguage = bab_getLanguage();
            }

            if (!$this->translationsExists($this->userLanguage)) {
                $this->userLanguage = $this->defaultLanguage;

                if (!$this->translationsExists($this->userLanguage)) {
                    unset($this->adapters[$this->addonName]);
                    return null;
                }
            }

            $file = $this->getLangPath().'lang-'.$this->userLanguage.'.mo';
            $this->adapters[$this->addonName] = new Zend_Translate('gettext', $file, $this->userLanguage);
        }

        return $this->adapters[$this->addonName];
    }



    /**
     * Translate
     *
     * @param string $str           Text to translate or singular form
     * @param string $str_plurals   Plurals form string
     * @param int $number           Number of items for plurals
     *
     * @return string
     */
    public function translate($str, $str_plurals = null, $number = null)
    {

        $adapter = $this->getAdapter();

        if (!isset($adapter)) {
            // no adapter, use a hack for english plural form
            if (null === $str_plurals) {
                return $str;
            }
            return $number == 1 ? $str : $str_plurals;
        }

        if (isset($str_plurals)) {
            $translated = $adapter->_(array($str, $str_plurals, $number));
        } else {
            $translated = $adapter->_($str);
        }

        return bab_getStringAccordingToDatabase($translated, 'UTF-8');
    }
}
